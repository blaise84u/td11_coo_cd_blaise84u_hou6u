package test;

import static org.junit.Assert.*;

import java.io.FileNotFoundException;

import org.junit.Test;

import XML.ChargeurMagasin;
import donnees.Magasin;

/**
* @author HOU Yunan
* @version 2018å¹´5æœˆ22æ—¥ ä¸‹å�ˆ2:57:57
*/
public class ChargeurMagasinTest {

	@Test
	public void testChargerMagasin() {
		boolean load=true;
		String repertoire = "musicbrainzSimple/";
		ChargeurMagasin charge = new ChargeurMagasin(repertoire);
		try {
			Magasin resultat = charge.chargerMagasin();
			load=true;
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			load=false;
		}

		assertEquals (" le répertoire n'est pas valide",true,load);
	}


}
