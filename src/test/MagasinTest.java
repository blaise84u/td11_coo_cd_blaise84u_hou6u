package test;

import static org.junit.Assert.*;

import java.io.FileNotFoundException;

import org.junit.Test;

import donnees.CD;
import donnees.ComparateurAlbum;
import donnees.Magasin;
import XML.ChargeurMagasin;

public class MagasinTest {

	@Test
	public void testTriCD() throws FileNotFoundException {
		Magasin resultat = new Magasin();
		ComparateurAlbum compare= new ComparateurAlbum();
		resultat.ajouteCd(new CD("a","a"));
		resultat.ajouteCd(new CD("c","c"));
		resultat.ajouteCd(new CD("b","b"));
		resultat.trier(compare);
		CD c1=resultat.getCd(0);
		CD c2=resultat.getCd(2);
		assertEquals (" Test du premier �lement de la liste ",c1.toString(),"--------------------------------------\na - a (0 pistes)\n--------------------------------------\n");
		assertEquals (" Test du dernier element de la liste ",c2.toString(),"--------------------------------------\nc - c (0 pistes)\n--------------------------------------\n");
	}

}
